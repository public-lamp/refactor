<?php

return [

    /*
   |--------------------------------------------------------------------------
   | Application Authorization Roles Handling
   |--------------------------------------------------------------------------
   |
   | Provides the users role names authorizations
   |
   */

    'role_ids' => [
        'user' => env('USER_ROLE_ID'),
        'admin' => env('ADMIN_ROLE_ID'),
        'super_admin' => env('SUPERADMIN_ROLE_ID')
    ],

];
